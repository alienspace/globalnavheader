import reactCSS from 'reactcss';
export const styles = reactCSS({
  'default': {
    MenuContainer: {
      height: 'auto',
      zIndex: 8,
      textAlign: 'left'
    },
    MenuItem: {
      padding: 0,
      margin: '0 auto'
    },
    MenuItemLI: {
      display:'inline-block'
    }
  }
});
