import React, { Component } from 'react';
import {
  Grid, Col, Button,
} from 'react-bootstrap';
import { IndexLink, Link } from 'react-router';
import { nextStorage } from './../../../util/nextStorage';
import { Auth } from './../../../auth.core';
import firebase from 'firebase';

/** CUSTOM COMPONENTS **/
// import { APP_CONFIG } from './../../../boot.config';
import { TOOLS } from './../../../util/tools.global';
import { styles } from './NavHeader.style';
import { Logo } from './../Logo/Logo';
// import { Cart } from './../../Shop/Cart/Cart';
import { CartBox } from './../../Shop/CartBox/CartBox';
import { LoginBox } from './../../Shop/LoginBox/LoginBox';
export class NavHeader extends Component{
  constructor(props){
    super(props);
    this.state = {
      appID : document.querySelector('[data-js="root"]').dataset.appid || null,
      products : [],
      categories : [],
      pages : [],

      currentUser: this.props.currentUser,
      navMobileOpened: this.props.navMobileOpened,
      navSidebar: this.props.sidebar,
      currentMenu: this.props.currentMenu,
      cartListHover: false,
      itemsNavDashboard: [
        {
          id: 10,
          title: { rendered: 'Apps' },
          slug: 'apps',
          icon: 'th',
          disabled: false
        },
        {
          id: 0,
          title: { rendered: 'Produtos' },
          slug: 'products',
          icon: 'shopping-cart',
          disabled: false
        },
        {
          id: 1,
          title: { rendered: 'Conteúdo' },
          slug: 'pages',
          icon: 'file-o',
          disabled: false
        },
        {
          id: 2,
          title: { rendered: 'Categorias' },
          slug: 'categories',
          icon: 'thumb-tack',
          disabled: false
        },
        {
          id: 3,
          title: { rendered: 'Usuários' },
          slug: 'users',
          icon: 'users',
          disabled: true
        },
        {
          id: 4,
          title: { rendered: 'Configurações' },
          slug: 'settings',
          icon: 'cogs',
          disabled: false
        }
      ],
      itemsNav: [
        {
          id: 0,
          title: 'A Empresa',
          slug: 'a-empresa',
          icon: 'shopping-cart',
          disabled: false
        },
        {
          id: 1,
          title: 'Produtos',
          slug: 'produtos',
          icon: 'shopping-cart',
          disabled: false
        },
        {
          id: 2,
          title: 'Contato',
          slug: 'contato',
          icon: 'shopping-cart',
          disabled: false
        },
        {
          id: 3,
          title: 'Blog',
          slug: 'blog',
          icon: 'shopping-cart',
          disabled: false
        }
      ]
    }

    this.onClickReturnFalse = this.onClickReturnFalse.bind(this);
    this.onClickMobile = this.onClickMobile.bind(this);
    this.renderLogo = this.renderLogo.bind(this);
    this.renderSearch = this.renderSearch.bind(this);
    this.renderNavDashboard = this.renderNavDashboard.bind(this);
    this.renderSidebarDashboard = this.renderSidebarDashboard.bind(this);

    this.renderNavItems = this.renderNavItems.bind(this);
    this.renderNavCategories = this.renderNavCategories.bind(this);
    this.renderNav = this.renderNav.bind(this);
    this.openNavMobile = this.openNavMobile.bind(this);
    this.renderCart = this.renderCart.bind(this);
    this.renderUserIcon = this.renderUserIcon.bind(this);
    this.onClickTargetBlank = TOOLS.onClickTargetBlank.bind(this);
    this.renderSocialNetwork = this.renderSocialNetwork.bind(this);
    this.renderSidebar = this.renderSidebar.bind(this);

    //  this.currentUser = this.currentUser.bind(this);
    this.signOut = this.signOut.bind(this);
  }

  componentWillMount(){
    let appData = firebase.database().ref('apps/'+this.state.appID+'/appData');
    appData.once('value').then((app) => {
      let APP = app.val();
      this.setState({
        company_name : APP.company_name,
        company_slogan_name : APP.company_slogan_name,
        company_fantasy_name : APP.company_fantasy_name,
        company_razao_social : APP.company_razao_social,
        company_cnpj : APP.company_cnpj,
        company_ie : APP.company_ie,
        company_address_zipcode : APP.company_address_zipcode,
        company_address : APP.company_address,
        company_address_number : APP.company_address_number,
        company_address_comp : APP.company_address_comp,
        company_address_bairro : APP.company_address_bairro,
        company_address_city : APP.company_address_city,
        company_address_state : APP.company_address_state,
        company_email : APP.company_email,
        company_telephone : APP.company_telephone,
        company_telephone2 : APP.company_telephone2,
        company_telephone_whatsapp : APP.company_telephone_whatsapp,
        company_skype : APP.company_skype,

        company_facebook : APP.company_facebook,
        company_instagram : APP.company_instagram,
        company_youtube : APP.company_youtube,
        company_twitter : APP.company_twitter,
        company_pinterest : APP.company_pinterest,
        company_linkedin : APP.company_linkedin,
        company_blog : APP.company_blog
      });
    });
  }//componentWillMount();

  componentDidMount(){

  };//componentDidMount();

  componentWillReceiveProps(nextProps){
    // console.log(nextProps);
    if(nextProps.currentMenu !== this.state.currentMenu){
      if(
        nextProps.currentMenu.split('/')[1] === 'blog'
        // eslint-disable-next-line
        && nextProps.currentMenu.split('/').length > 2
        // eslint-disable-next-line
        || nextProps.currentMenu.split('/')[1] === 'home'
      ){
        this.setState({
          currentMenu: nextProps.currentMenu
        });
      }else{
        this.setState({
          navMobileOpened: !this.state.navMobileOpened,
          currentMenu: nextProps.currentMenu
        });
      }
    }
  }//componentWillReceiveProps(nextProps);

  onClickReturnFalse(e){
    e.preventDefault();
    return false;
  }//onClickReturnFalse(e)

  onClickMobile(e) {
    if(e)
      e.preventDefault();
    let viewport = window.innerWidth;
    if(viewport <= 768)
      this.setState({ navMobileOpened: !this.state.navMobileOpened });
  };//onClickMobile

  classNavHeader(){
    if(this.state.navMobileOpened){
      return 'navHeader navMobileOpened no-padding';
    }

    if( this.state.navSidebar && this.props.dashboard ){
      if( this.props.currentUser ){
        return 'navHeader navSidebar no-padding';
      }else{
        return 'navHeader navSidebar hidden no-padding';
      }
    }else if( !this.props.dashboard && this.state.navSidebar ){
      return 'navHeader hidden-lg no-padding';
    }

    return 'navHeader no-padding';
  };//classNavHeader

  renderLogo(){
    if(this.props.dashboard){
      if( this.props.isMobile ){
        return (<Logo style={{width:'70%',margin:'5px auto'}} className="logoNavMobile" dashboard />);
      }else{
        return (
          <Col xs={12} md={2}>
            <Logo className="logoHeader" dashboard />
          </Col>
        );
      }
    }else{
      if( this.props.isMobile ){
        return (<Logo style={{width:'70%',margin:'5px auto'}} className="logoNavMobile" src={'/assets/images/logo_volare.png'} />);
      }else{
        return (
          <Col xs={12} md={3} className={'logoHeaderContainer text-center no-padding'}>
            <Logo className={'logoHeader'} src={'/assets/images/logo_volare.png'} />
          </Col>
        );
      }
    }
  };//renderLogo();

  renderSearch(){
    if( this.props.withSearch && this.props.packages.SHOP.searchBox ){
      return (
        <Col xs={12} md={6} className={'searchTop'}>
          { this.props.withSearchTop ?
            <div>
              <ul>
                <li><Link to={'#'}>Central de Atendimento</Link></li>
                <li><Link to={'#'}>Meus Pedidos</Link></li>
                <li><Link to={'#'}>Perguntas Frequentes</Link></li>
              </ul>
            </div>
          :
            ''
          }
          <Col xs={12} md={12} className={'input-group'}>
            <Col xs={12} md={12} className={'no-padding'}>
              <Col xs={11} md={10} className={'no-padding col-md-offset-1'}>
                <input
                  ref={'searchInput'}
                  className={'searchInput input-form'}
                  type={'text'}
                  placeholder={'Digite aqui o que você procura..'}
                />
              </Col>
              <div className={'no-padding searchBtnContainer col-xs-1'}>
                <i className={'fa fa-search'}></i>
                <input
                  ref={'searchBtn'}
                  className={'searchBtn input-form'}
                  type={'button'}
                />
              </div>
            </Col>
          </Col>
        </Col>
      );
    }
  }//renderSearch();

  signOut(e){
    e.preventDefault();
    // Sign out
    firebase.auth().signOut();
  };//signOut

  renderNavDashboard(){
    // eslint-disable-next-line
    const HOME = () => {
      return (<li key={0}><IndexLink to={{pathname:'/dashboard/'}} activeClassName="active"> <span>Home</span></IndexLink></li>);
    }

    return (
      <Col xs={12} md={8} className={'no-padding'}>
        { this.props.currentUser ?
          <ul style={{margin:'0 auto',padding:'0 3px'}}>
            {/*HOME()*/}
            {/*
              this.state.itemsNavDashboard.map((item) => {
                if(this.state.currentMenu == '/'+item.slug){
                  return (<li key={item.id}><Link className={'fa fa-hashtag'} to={{pathname:'/'+item.slug}} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
                }else{
                  return (<li key={item.id}><Link to={{pathname:'/'+item.slug}} title={item.title.rendered} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
                }
              }) */
            }
            <li key={'user'}><Link className={'fa fa-user-circle-o'} to={'/dashboard/logout'} onClick={this.signOut}> <span>Admin - {this.props.currentUser.email}</span></Link></li>
            <li key={'shopping'}><Button className={'fa fa-shopping-bag label-info'} onClick={() =>{window.location = '/';}} style={{padding:'5px 10px',fontSize:12}}> <span>Meu Site</span></Button></li>
            <li key={'exit'}><Link className={'fa fa-sign-out label-danger'} to={'/dashboard/logout'} style={{padding:'5px 10px',fontSize:12}} onClick={this.signOut}> <span>Sair</span></Link></li>
          </ul>
        :
          <Auth
            modeForm={'horizontal'}
            currentUser={this.props.currentUser}
          />
        }
      </Col>
    );
  };//renderNavDashboard

  renderSidebarDashboard(){
    if( this.props.currentUser ){
      return (
        <ul style={styles.MenuItem} className={'navSidebar'}>
          <li key={0}><IndexLink className={'fa fa-home'} to={{pathname:'/dashboard/'}} activeClassName="active"> <span>Home</span></IndexLink></li>
        { this.state.itemsNavDashboard.map((item) => {
            if(this.state.currentMenu === '/dashboard/'+item.slug){
              return (<li key={item.id}><Link className={!item.disabled ? 'fa fa-'+item.icon : 'disabled fa fa-'+item.icon} to={{pathname:'/dashboard/'+item.slug}} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
            }else{
              return (<li key={item.id}><Link className={!item.disabled ? 'fa fa-'+item.icon : 'disabled fa fa-'+item.icon} to={{pathname:'/dashboard/'+item.slug}} title={item.title.rendered} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
            }
          })
        }
        <li key={'exit'}><Link className={'fa fa-sign-out'} to={'/dashboard/logout'} style={{padding:'5px 20px',fontSize:12}} onClick={this.signOut}> <span>Sair</span></Link></li>
        </ul>
      );
    }
  };//renderSidebarDashboard();

  renderNavItems(){
    /*
    if(this.props.categories && this.props.categories.length > 0){
      return this.props.categories.map((category) => {
        if(this.state.currentMenu === '/'+category.slug){
          return (<li key={category.categoryID}><Link className={'fa fa-hashtag'} to={{pathname:'/'+category.slug}} activeClassName="active"> <span>{category.title}</span></Link></li>);
        }else{
          return (<li key={category.categoryID}><Link to={{pathname:'/'+category.slug}} title={category.title} activeClassName="active"> <span>{category.title}</span></Link></li>);
        }
      });
    }
    */
    // if(this.props.pages && this.props.pages.length > 0){
      // eslint-disable-next-line
      return this.state.itemsNav.map((page,key) => {
        if(this.state.currentMenu === '/'+page.slug){
          if(this.state.navMobileOpened)
            return (<li key={key} onClick={this.onClickMobile}><Link to={{pathname:'/'+page.slug}} activeClassName="active"> <span>{page.title}</span></Link></li>);
          return (<li key={key}><Link to={{pathname:'/'+page.slug}} activeClassName="active"> <span>{page.title}</span></Link></li>);
        }else{
          if(this.state.isMobile)
            return (<li key={key} onClick={this.onClickMobile}><Link to={{pathname:'/'+page.slug}} title={page.title} activeClassName="active"> <span>{page.title}</span></Link></li>);
          return (<li key={key}><Link to={{pathname:'/'+page.slug}} title={page.title} activeClassName="active"> <span>{page.title}</span></Link></li>);
        }
      });
      /*return this.props.pages.map((page) => {
        if(page.typeContent === 'Page'){
          if(this.state.currentMenu === '/'+page.slug){
            if(this.state.navMobileOpened)
              return (<li key={page.pageID} onClick={this.onClickMobile}><Link to={{pathname:'/'+page.slug}} activeClassName="active"> <span>{page.title}</span></Link></li>);
            return (<li key={page.pageID}><Link to={{pathname:'/'+page.slug}} activeClassName="active"> <span>{page.title}</span></Link></li>);
          }else{
            if(this.state.isMobile)
              return (<li key={page.pageID} onClick={this.onClickMobile}><Link to={{pathname:'/'+page.slug}} title={page.title} activeClassName="active"> <span>{page.title}</span></Link></li>);
            return (<li key={page.pageID}><Link to={{pathname:'/'+page.slug}} title={page.title} activeClassName="active"> <span>{page.title}</span></Link></li>);
          }
        }

        // else if(page.typeContent === 'Post'){
        //   if(this.state.currentMenu === '/'+page.slug){
        //     return (<li key={page.pageID}><Link className={'fa fa-hashtag'} to={{pathname:'/'+page.slug}} activeClassName="active"> <span>{page.category}</span></Link></li>);
        //   }else{
        //     return (<li key={page.pageID}><Link className={'hasParent'} to={{pathname:'/'+page.slug}} title={page.title} activeClassName="active"> <span>{page.category}</span></Link></li>);
        //   }
        // }
      }); */
    // }
  }//renderNavItems();

  renderNavCategories(){
    if(this.props.categories && this.props.categories.length > 0){
      // eslint-disable-next-line
      return this.props.categories.map((category) => {
        if(category.parent === 'Categoria Master'){
          // let submenu = () => {
          //   return this.props.pages.map((page) => {
          //     if(page.typeContent === 'Post' && page.category === category.title){
          //       return (<li key={page.pageID}><Link to={{pathname:'/'+page.slug}} activeClassName="active"><span>{page.title}</span></Link></li>);
          //     }
          //   });
          // }


          let categoryProducts = () => {
            // eslint-disable-next-line
            return this.props.products.map((product) => {
              if(product.category === category.title){
                // return (<li key={product.productID}><Link to={{pathname:'/'+category.slug+'/'+product.slug}} activeClassName="active"><span>{product.title}</span></Link></li>);
                return (<li key={product.productID}><Link to={{pathname:'/product/'+product.slug}} activeClassName="active"><span>{product.title}</span></Link></li>);
              }
            });
          }

          let subCategories = () => {
            // eslint-disable-next-line
            return this.props.categories.map((subcategory) => {
              if(subcategory.parent === category.title){
                return (<li key={subcategory.categoryID}><Link to={{pathname:'/'+category.slug+'/'+subcategory.slug}} activeClassName="active"><span>{subcategory.title}</span></Link></li>);
              }
            });
          };


          if(this.state.currentMenu === '/'+category.slug){
            return (
              <li key={category.categoryID} className={'has-submenu'}>
                <Link className={'hasParent'} to={{pathname:'/'+category.slug}} activeClassName="active"> <span>{category.title}</span></Link>
                <ul className={'submenu'}>
                  <ul className={'level1'}>
                    {subCategories()}
                  </ul>
                </ul>
              </li>
            );
          }else{
            return (
              <li key={category.categoryID}>
                <Link
                  className={'hasParent'}
                  to={'#'}
                  title={category.title}
                  onClick={this.onClickReturnFalse}>
                    <span>{category.title}</span>
                </Link>
                <ul className={'submenu'}>
                  <ul className={'level1'}>
                    {categoryProducts()}
                    {subCategories()}
                  </ul>
                </ul>
              </li>
            );
          }
        }
      });
    }
  }//renderNavCategories();

  renderNav(){
    // eslint-disable-next-line
    const HOME = () => {
      return (<li key={0}><IndexLink to={{pathname:'/'}} activeClassName="active"> <span>Home</span></IndexLink></li>);
    }

    if( this.props.packages.SITE.navFull ){
      return (
        <Col xs={12} md={12} className={'no-padding navMenuContainer z-index6'}>
          <span className={'navIcon'} onClick={this.onClickMobile}></span>
          {this.renderSocialNetwork()}
          <Grid>
            <Col xs={12} md={12} className={'no-padding'}>
              { this.props.currentUser ?
                <ul style={{margin:'0 auto',padding:'0 3px'}}>
                  {HOME()}
                  {/*this.renderNavItems()*/}
                  {this.renderNavCategories()}
                  {/*<li key={'user'}><Link className={'fa fa-user-circle-o'} to={'/dashboard/logout'} onClick={this.signOut}> <span>Admin - {this.props.currentUser.email}</span></Link></li>*/}
                  {/*<li key={'shopping'}><Link className={'fa fa-shopping-bag label-info'} to={'/'} style={{padding:'5px 10px',fontSize:12}}> <span>Meu Site</span></Link></li>*/}
                </ul>
              :
              <ul style={{margin:'0 auto',padding:'0 3px'}}>
                {HOME()}
                {this.renderNavItems()}
              </ul>
              }
            </Col>
          </Grid>
        </Col>
      );
    }else{
      return (
        <Col xs={12} md={8} className={'no-padding navMenuContainer position-static pull-right'}>
          <span className={'navIcon'} onClick={this.onClickMobile}></span>
          {this.renderSocialNetwork()}
          <Grid className={'position-static'}>
            <Col xs={12} md={12} className={'no-padding position-static'}>
              { this.props.currentUser ?
                <ul className={'text-right'}>
                  {HOME()}
                  {/*this.renderNavItems()*/}
                  {this.renderNavCategories()}
                  {/*<li key={'user'}><Link className={'fa fa-user-circle-o'} to={'/dashboard/logout'} onClick={this.signOut}> <span>Admin - {this.props.currentUser.email}</span></Link></li>*/}
                  {/*<li key={'shopping'}><Link className={'fa fa-shopping-bag label-info'} to={'/'} style={{padding:'5px 10px',fontSize:12}}> <span>Meu Site</span></Link></li>*/}
                </ul>
              :
              /*<ul style={{margin:'0 auto',padding:'0 3px'}}>
                {HOME()}
                {this.renderNavItems()}
              </ul>*/
                ''
              }
            </Col>
          </Grid>
        </Col>
      );
    }
  };//renderNav();

  openNavMobile(){
    // let
      // menuList = document.querySelector('.navMenuContainer ul');

    // if(!menuList)
      // console.log(menuList)
      // menuList.classList.add('opened')
    // return menuList.classList.toggle('opened');
  }//openNavMobile();

  renderCart(){
    let
      nextDash = nextStorage.getJSON('nextDash');
    if( this.props.withCart && this.props.packages.SHOP.cart ){
      return (
        <Col xs={12} md={3} className={'cartContainer'}>
          <Col xs={2} md={2} className={'no-padding pull-right text-center'}>
            <div className={'cartHover'}>
              <Link to={'/carrinho'} className={'fa fa-shopping-cart'}>
                <span className={'countItems'}>{nextDash.CART_productsList.length}</span>
              </Link>
              <CartBox
                cartListHover
              />
            </div>
          </Col>
          <Col xs={2} md={8} className={'no-padding pull-right userHeaderContainer'}>
            {this.renderUserIcon()}
          </Col>
        </Col>
      );
    }
  }//renderCart();

  renderUserIcon(){
    return (
      <div className={'userHoverContainer'}>
        <Link to={'/login'} className={'fa fa-user-o'}>
          <span>Entre ou Cadastre-se</span>
        </Link>
        <LoginBox
          userHover
          currentUser={this.props.currentUser}
          isVisitor={this.props.isVisitor}
        />
      </div>
    );
  }//renderUserIcon();

  renderSocialNetwork(){
    if(this.props.socialnetwork && this.props.packages.SHOP.socialnetwork){
      if(!this.props.packages.SHOP.telephone){
        return (
          <Col xs={12} md={3} className={'socialNetwork navIconSocial pull-right'}>
            <ul>
              <li>
                <Link
                  to={this.state.company_facebook}
                  onClick={this.onClickTargetBlank}
                  title={'Siga-nos no Facebook'}
                  className={'fa fa-facebook'}>
                  <i></i>
                </Link>
              </li>
              <li>
                <Link
                  to={this.state.company_instagram}
                  onClick={this.onClickTargetBlank}
                  title={'Siga-nos no Instagram'}
                  className={'fa fa-instagram'}>
                  <i></i>
                </Link>
              </li>
            </ul>
          </Col>
        );
      }else{
        return (
          <Col xs={12} md={3} className={'socialNetwork navIconSocial pull-right'}>
            <h6 className={'telephoneHeader'}>{this.state.company_telephone || this.state.company_telephone2}</h6>
            <ul>
              <li>
                <Link
                  to={this.state.company_facebook}
                  onClick={this.onClickTargetBlank}
                  title={'Siga-nos no Facebook'}
                  className={'fa fa-facebook'}>
                  <i></i>
                </Link>
              </li>
              <li>
                <Link
                  to={this.state.company_instagram}
                  onClick={this.onClickTargetBlank}
                  title={'Siga-nos no Instagram'}
                  className={'fa fa-instagram'}>
                  <i></i>
                </Link>
              </li>
            </ul>
          </Col>
        );
      }
    }
  };//renderSocialNetwork();

  renderSidebar(){
    if( this.props.currentUser ){
      return (
        <ul style={styles.MenuItem} className={'navSidebar'}>
          <li key={0}><IndexLink className={'fa fa-home'} to={{pathname:'/'}} activeClassName="active"> <span>Home</span></IndexLink></li>
        { this.state.itemsNav.map((item) => {
            if(this.state.currentMenu === '/dashboard/'+item.slug){
              return (<li key={item.id}><Link className={'fa fa-'+item.icon} to={{pathname:'/dashboard/'+item.slug}} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
            }else{
              return (<li key={item.id}><Link className={'fa fa-'+item.icon} to={{pathname:'/dashboard/'+item.slug}} title={item.title.rendered} activeClassName="active"> <span>{item.title.rendered}</span></Link></li>);
            }
          })
        }
        <li key={'exit'}><Link className={'fa fa-sign-out'} to={'/dashboard/logout'} style={{padding:'5px 20px',fontSize:12}} onClick={this.signOut}> <span>Sair</span></Link></li>
        </ul>
      );
    }
  };//renderSidebar();

  render(){
    let renderView = () => {
      if( this.props.packages.SITE.navFull ){
        if( !this.state.navSidebar ){
          return (
            <div>
              <Grid className={'no-padding backgroundWhite position-relative z-index7'}>
                {this.renderLogo()}
                {this.renderSearch()}
                { this.props.packages.SHOP.cart ?
                  this.renderCart()
                :
                  this.renderSocialNetwork()
                }
              </Grid>
              {this.renderNav()}
            </div>
          );
        }else{
          if(this.props.isMobile){
            return (
              <Col xs={12} md={12} className={'no-padding'}>
                {this.renderSidebar()}
              </Col>
            );
          }
        }
      }else{
        if( !this.state.navSidebar ){
          return (
            <div>
              <Grid className={'no-padding'}>
                {this.renderLogo()}
                {this.renderSearch()}
                { this.props.packages.SHOP.cart ?
                  this.renderCart()
                :
                  this.renderSocialNetwork()
                }
                {this.renderNav()}
              </Grid>
            </div>
          );
        }else{
          if(this.props.isMobile){
            return (
              <Col xs={12} md={12} className={'no-padding'}>
                {this.renderSidebar()}
              </Col>
            );
          }
        }
      }
    }

    let classNavHeader = this.classNavHeader();
    return (
      <Col xs={12} md={this.state.navSidebar ? 2 : 12} style={styles.MenuContainer} className={classNavHeader}>
        {renderView()}
      </Col>
    );
  }
}
